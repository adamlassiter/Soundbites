package com.projectvuvuzela.soundbites;

import android.app.Activity;
import android.media.*;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.projectvuvuzela.soundbites.views.AudioController;

import java.io.IOException;

import static android.media.session.PlaybackState.*;

public class SoundbiteStreamer {

    private Activity currentActivity;

    private MediaPlayer player = new MediaPlayer();
    private MediaSession session;
    private PlaybackState.Builder state = new PlaybackState.Builder();
    private AudioController controller;

    private boolean isPrepared = false;
    private int buffer = 0;

    private MediaPlayerCallbackHandler playerCallbackHandler = new MediaPlayerCallbackHandler();
    private AudioControllerCallbackHandler controllerCallbackHandler = new AudioControllerCallbackHandler();

    private static SoundbiteStreamer ourInstance = new SoundbiteStreamer();


    public static SoundbiteStreamer getInstance() {
        return ourInstance;
    }

    private SoundbiteStreamer() {
        // Nothing to do, except prevent other instances from being constructed
    }

    public void setCurrentActivity(Activity currentActivity) {
        session = new MediaSession(currentActivity, "Soundbites");
        session.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        state.setActions(ACTION_PLAY_PAUSE | ACTION_FAST_FORWARD | ACTION_REWIND | ACTION_SEEK_TO);
        session.setPlaybackState(state.build());
        this.currentActivity = currentActivity;
    }

    public void setController(AudioController controller) {
        this.controller = controller;
        controller.setCallbackHandler(new AudioControllerCallbackHandler());

        if (isPrepared) {
            controller.setDuration(player.getDuration());
            controller.setProgress(player.getCurrentPosition());
            controller.setBuffer(buffer);
            controller.setPlaying(true);
            controller.setEnabled(true);
        }

    }

    public void playSoundbite(String audioURL) {
        session.setCallback(new MediaSessionCallbackHandler());
        session.setActive(false);
        session.setMetadata(new MediaMetadata.Builder().putString(MediaMetadata.METADATA_KEY_TITLE, "The Title").build());

        player.reset();
        isPrepared = false;

        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            player.setDataSource(audioURL);

        } catch (IOException | IllegalArgumentException e) {
            Snackbar.make(controller, "Couldn't play soundbite: " + e.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
            return;
        }

        player.setOnPreparedListener(playerCallbackHandler);
        player.setOnBufferingUpdateListener(playerCallbackHandler);
        player.setOnCompletionListener(playerCallbackHandler);
        player.setOnInfoListener(playerCallbackHandler);
        player.setOnErrorListener(playerCallbackHandler);

        player.prepareAsync();

        controller.setEnabled(false);
        controller.setPlaying(true);
        controller.setProgress(0);
        controller.setDuration(0);
    }

    private class MediaPlayerCallbackHandler implements MediaPlayer.OnPreparedListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnInfoListener, MediaPlayer.OnErrorListener {

        public void onPrepared(MediaPlayer mp) {
            isPrepared = true;
            session.setActive(true);
            session.setPlaybackState(state.setState(STATE_PLAYING, 0, 1.0f).build());
            currentActivity.runOnUiThread(new Runnable() {
                public void run() {
                    controller.setDuration(player.getDuration());
                    controller.setEnabled(true);
                    controller.setPlaying(true);
                }
            });
            player.start();
        }

        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            controller.setBuffer(percent);
            buffer = percent;
        }

        public void onCompletion(MediaPlayer mp) {
            isPrepared = false;
            session.setActive(false);

            currentActivity.runOnUiThread(new Runnable() {
                public void run() {
                    controller.setEnabled(false);
                    controller.setPlaying(false);
                }
            });
        }

        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            switch (what) {
                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                    return true;

                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                    return true;
            }
            return false;
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.e("SoundbiteStreamer", "What even: " + what + " - " + extra);

            String error;

            switch (what) {
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    error = "Apparently, the server died.";
                    break;

                default:
                case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                    error = "An unknown error occurred.";
                    break;
            }

            Snackbar.make(controller, "Couldn't play soundbite: " + error, Snackbar.LENGTH_LONG).show();
            if (session != null) {
                session.setActive(false);
            }
            return true;
        }
    }

    private class AudioControllerCallbackHandler implements AudioController.CallbackHandler {
        public void start() {
            player.start();
        }

        public void pause() {
            player.pause();
        }

        public int getPosition() {
            return player.getCurrentPosition();
        }

        public void seekTo(int pos) {
            player.seekTo(pos);
        }
    }

    private class MediaSessionCallbackHandler extends MediaSession.Callback {
        public void onPlay() {
            player.start();
        }

        public void onPause() {
            player.pause();
        }

        public void onFastForward() {
            player.seekTo(Math.min(player.getCurrentPosition() + 5000, player.getDuration()));
        }

        public void onRewind() {
            player.seekTo(Math.max(player.getCurrentPosition() - 5000, 0));
        }

        public void onSeekTo(long pos) {
            player.seekTo((int)pos);
        }

    }

}
