package com.projectvuvuzela.soundbites.networking;

import android.content.SharedPreferences;

import com.projectvuvuzela.soundbites.App;
import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.LoginData;

import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;
import static com.android.volley.Request.Method.*;
import static com.projectvuvuzela.soundbites.networking.NetworkHandler.*;

/**
 * Interacts with the server's /login API.
 * Access these methods like so: NetworkHandler.login.login(...)
 */
public class LoginRequestHandler {

    private NetworkHandler network;

    protected LoginRequestHandler(NetworkHandler networkHandler) {
        network = networkHandler;
    }

    public void login(String username, String password, ResponseHandler<LoginData> handler) {
        login(username, password, true, handler);
    }

    public void login(final String username, String password, boolean handleResponse, ResponseHandler<LoginData> handler) {
        if (handleResponse) {
            final ResponseHandler<LoginData> userHandler = handler;

            handler = new ResponseHandler<LoginData>() {
                public void onResponse(boolean success, LoginData response, String error) {
                    if (success) {
                        NetworkHandler.setUserID(response.userID, username, response.authToken);
                    }

                    userHandler.onResponse(success, response, error);
                }
            };
        }

        ObjectRequest request = new ObjectRequest<>(PUT, BASE_URL + "/login", LoginData.class, handler);
        request.putInHeader("user_nm", username);
        request.putInHeader("passwd", password);

        network.queue.add(request);
    }

    public void logout(ResponseHandler<String> handler) {
        logout(NetworkHandler.getUserID(), NetworkHandler.getAuthToken(), true, handler);
    }

    public void logout(String userID, String authToken, boolean handleResponse, ResponseHandler<String> handler) {
        if (handleResponse) {
            final ResponseHandler<String> userHandler = handler;

            handler = new ResponseHandler<String>() {
                public void onResponse(boolean success, String response, String error) {
                    if (success) {
                        SharedPreferences.Editor prefs = App.getContext().getSharedPreferences(PREFS, MODE_PRIVATE).edit();
                        prefs.remove(PREFS_USER_ID);
                        prefs.remove(PREFS_AUTH_TOKEN);
                        prefs.apply();
                    }

                    userHandler.onResponse(success, response, error);
                }
            };
        }

        StringHeaderRequest request = new StringHeaderRequest(DELETE, BASE_URL + "/login", handler);
        request.putInHeader("user_id", userID);
        request.putInHeader("auth_tk", authToken);

        network.queue.add(request);
    }

}
