package com.projectvuvuzela.soundbites.views;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.*;

import com.projectvuvuzela.soundbites.*;
import com.projectvuvuzela.soundbites.activities.*;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.data.UserData;

public class FindUserDialog extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());

        final Activity activity = getActivity();
        final FindUserView view = new FindUserView(getContext());
        dialogBuilder.setView(view);

        dialogBuilder.setPositiveButton("Show Profile", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                NetworkHandler.user.getPostsForUsername(view.getUsername(), new NetworkHandler.ResponseHandler<UserData>() {
                    public void onResponse(boolean success, UserData response, String error) {
                        if (success) {
                            Intent intent = new Intent(App.getContext(), ProfileActivity.class);
                            intent.putExtra(ProfilePage.USER_NAME_KEY, response.username);
                            intent.putExtra(ProfilePage.USER_ID_KEY, response.userID);
                            activity.startActivity(intent);

                        } else {
                            Snackbar.make(activity.findViewById(R.id.container), error, Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        dialogBuilder.setNegativeButton("Cancel", null);

        return dialogBuilder.create();
    }

    class FindUserView extends LinearLayout {

        private TextView instructionText;
        private EditText usernameField;

        public FindUserView(Context context) {
            super(context);
            setUp();
        }

        private void setUp() {
            setOrientation(VERTICAL);
            int padding = Utils.dpToPx(15, getContext());
            setPadding(padding, padding, padding, 0);

            instructionText = new TextView(getContext());
            instructionText.setText("Whose profile would you like to see?");
            addView(instructionText);

            usernameField = new EditText(getContext());
            usernameField.setHint("Enter a username");
            addView(usernameField);
        }

        public String getUsername() {
            return usernameField.getText().toString();
        }

    }
}
