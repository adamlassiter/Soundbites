# Soundbites
**A social app for sharing music** *- Group 13 - Integrated Group Project (CM20257)*

## Contributing
For more information on how to contribute, using Git Flow, check [CONTRIBUTING.md](CONTRIBUTING.md).

## Dependencies
### Server
The server currently requires:
- mongodb
- python3
    - flask, flask_restful, pyopenssl, pymongo
- ffmpeg

### Android
It's recommended to build the Android app using [Android Studio](https://developer.android.com/studio/index.html). Any dependencies will be automatically downloaded on build.

## Authors
- Liam Berrisford
- João Duarte
- Ben Hetherington
- Matthew Hewitt
- Adam Lassiter
- Venislav Venkov
