#!/usr/bin/python3

from hashlib import sha256
from uuid import uuid1, uuid4
from time import time as now
from random import choice
from bson.json_util import loads

SALT_LEN = 32


class User:

    def __init__(self, _id='', user_nm='', passwd='', salt='', auth_tk='',
                 post_ids=[], friend_ids=[]):
        self.user_id = _id if _id else str(uuid1())
        self.user_nm = user_nm
        if salt:
            self.passwd = passwd
            self.salt = salt
        else:
            self.salt = User.salt_gen()
            self.passwd = User.hash_password(passwd, self.salt)
        self.auth_tk = auth_tk
        self.post_ids = post_ids
        self.friend_ids = friend_ids

    def to_dict_public(self):
        return {'_id': self.user_id, 'user_nm': self.user_nm,
                'post_ids': self.post_ids, 'friend_ids': self.friend_ids}

    def to_dict(self):
        return {'_id': self.user_id, 'user_nm': self.user_nm,
                'passwd': self.passwd, 'salt': self.salt,
                'auth_tk': self.auth_tk,
                'post_ids': self.post_ids, 'friend_ids': self.friend_ids}

    @classmethod
    def from_dict(cls, dict):
        if dict is not None:
            usr_keys = ['_id', 'user_nm', 'passwd', 'salt', 'auth_tk',
                        'post_ids', 'friend_ids']
            if set(usr_keys) == set(dict.keys()):
                return cls(**dict)
            elif set(usr_keys).issuperset(set(dict.keys())):
                # user is not up-to-date with db structure, this is okay
                return cls(**dict)
            else:
                raise Exception('Invalid Dict representation of User object')
        else:
            return None

    @classmethod
    def from_json(cls, json):
        if json is not None:
            return cls.from_dict(loads(json))
        else:
            return None

    @staticmethod
    def salt_gen():
        return ''.join([chr(choice(range(33, 127))) for _ in range(SALT_LEN)])

    @staticmethod
    def hash_password(password, salt):
        h = sha256()
        h.update((password + salt).encode('utf-8'))
        return h.hexdigest()

    def verify_password(self, password):
        return User.hash_password(password, self.salt) == self.passwd

    def set_auth_tk(self):
        self.auth_tk = str(uuid4())
        return self.auth_tk

    def unset_auth_tk(self):
        self.auth_tk = ''


class Post:

    def __init__(self, _id=0, user_id='', post_nm='', file_nm='',
                 comment_ids=[], likes=[], time=0, file_desc=None, img_art=None):
        self.post_id = _id if _id else str(uuid1())
        self.user_id = user_id
        self.user_nm = UserDB().get_user(user_id).user_nm
        self.post_nm = post_nm
        self.file_nm = file_nm
        self.comment_ids = comment_ids
        self.likes = likes
        self.time = time if time else now()
        self.file_desc = file_desc
        self.img_art = img_art

    def to_dict(self):
        return {'_id': self.post_id, 'user_id': self.user_id,
                'user_nm': self.user_nm, 'post_nm': self.post_nm,
                'file_nm': self.file_nm, 'comment_ids': self.comment_ids,
                'likes': self.likes, 'time': self.time,
                'file_desc': self.file_desc, 'img_art': self.img_art}

    @classmethod
    def from_dict(cls, dict):
        if dict is not None:
            post_keys = ['_id', 'user_id', 'post_nm', 'file_nm', 'comment_ids',
                         'likes', 'time', 'file_desc', 'img_art']
            dict.pop('user_nm', None)
            if set(post_keys) == set(dict.keys()):
                return cls(**dict)
            elif set(post_keys).issuperset(set(dict.keys())):
                # post is not up-to-date with db structure, this is okay
                return cls(**dict)
            else:
                raise Exception('Invalid Dict representation of Post object')
        else:
            return None

    @classmethod
    def from_json(cls, json):
        if json is not None:
            return cls.from_dict(loads(json))
        else:
            return None


class Comment:

    def __init__(self, _id=0, post_id='', user_id='', comment='', time=0):
        self.comment_id = _id if _id else str(uuid1())
        self.post_id = post_id
        self.user_id = user_id
        self.user_nm = UserDB().get_user(user_id).user_nm
        self.comment = comment
        self.time = time if time else now()

    def to_dict(self):
        return {'_id': self.comment_id, 'post_id': self.post_id,
                'user_id': self.user_id, 'user_nm': self.user_nm,
                'comment': self.comment, 'time': self.time}

    @classmethod
    def from_dict(cls, dict):
        if dict is not None:
            comment_keys = ['_id', 'post_id', 'user_id', 'comment', 'time']
            dict.pop('user_nm', None)
            if set(comment_keys) == set(dict.keys()) or set(comment_keys).issuperset(set(dict.keys())):
                return cls(**dict)
            elif set(post_keys).issuperset(set(dict.keys())):
                # post is not up-to-date with db structure, this is okay
                return cls(**dict)
            else:
                raise Exception(
                    'Invalid Dict representation of Comment object')
        else:
            return None

    @classmethod
    def from_json(cls, json):
        if json is not None:
            return cls.from_dict(loads(json))
        else:
            return None

from dbInterface import UserDB
