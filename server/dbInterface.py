#!/usr/bin/python3

from flask import make_response
from pymongo import MongoClient

ADDR, PORT = 'localhost', 27017
CLIENT = MongoClient(ADDR, PORT)
DB = CLIENT['test']


class UserDB:

    def __init__(self):
        self.db = DB.users

    def _update_user(self, user):
        self.db.update({'_id': user.user_id}, {'$set': user.to_dict()})

    # given a username, return the corresponding user id
    def get_user_by_name(self, user_nm):
        user = User.from_dict(self.db.find_one({'user_nm': user_nm}))
        return user

    # given a user id, get the public view of the user
    def get_user(self, user_id):
        user = User.from_dict(self.db.find_one({'_id': user_id}))
        return user

    # add the user 'user_nm' to the database, with (hashed) password and salt
    def create_user(self, user_nm, passwd, salt):
        if self.get_user_by_name(user_nm) is None:
            user = User(user_nm=user_nm, passwd=passwd, salt=salt)
            self.db.insert_one(user.to_dict())
        else:
            user = None
        return user

    # get the i->j most recent posts by a user
    def get_recent_posts(self, user, i, j):
        post_ids = user.post_ids[-j: -i - 1] if i > 1 else user.post_ids[-j:]
        posts = map(PostDB().get_post, post_ids)
        return posts

    def add_friend(self, user, friend, auth_tk):
        if user.auth_tk != auth_tk:
            return make_response('Access denied', 401)
        if friend.user_id not in user.friend_ids:
            user.friend_ids.append(friend.user_id)
            self._update_user(user)
        return user

    def get_friends(self, user):
        friends = map(self.get_user, user.friend_ids)
        return friends

    # 'log-in' a user and return an auth token
    def set_auth_token(self, user):
        user.set_auth_tk()
        self._update_user(user)
        return user.auth_tk

    def unset_auth_token(self, user):
        user.unset_auth_tk()
        self._update_user(user)
        return user.auth_tk


class PostDB:

    def __init__(self):
        self.db = DB.posts

    def _update_post(self, post):
        self.db.update({'_id': post.post_id}, {'$set': post.to_dict()})

    # get a post post_id
    def get_post(self, post_id):
        post = Post.from_dict(self.db.find_one({'_id': post_id}))
        return post

    # like a post by a given user
    def like_post(self, post, user, auth_tk):
        if user.auth_tk != auth_tk:
            return make_response('Access denied', 401)
        if user.user_id not in post.likes:
            post.likes.append(user.user_id)
        self._update_post(post)
        return post

    # create a new post by user of post_content
    # if and only if a valid auth token is given
    def create_post(self, user, post_nm, file_nm, auth_tk, file_desc, img_art):
        if user.auth_tk != auth_tk:
            return make_response('Access denied', 401)
        post = Post(user_id=user.user_id, post_nm=post_nm, file_nm=file_nm, file_desc=file_desc, img_art=img_art)
        user.post_ids.append(post.post_id)
        UserDB()._update_user(user)
        self.db.insert_one(post.to_dict())
        return post


class CommentDB:

    def __init__(self):
        self.db = DB.comments

    def _update_comment(self, comment):
        self.db.update({'_id': comment.comment_id},
                       {'$set': comment.to_dict()})

    def get_comment(self, comment_id):
        comment = Comment.from_dict(self.db.find_one({'_id': comment_id}))
        return comment

    def create_comment(self, post, user, comment, auth_tk):
        if user.auth_tk != auth_tk:
            return make_response('Access denied', 401)
        comment = Comment(post_id=post.post_id, user_id=user.user_id,
                          comment=comment)
        post.comment_ids.append(comment.comment_id)
        PostDB()._update_post(post)
        self.db.insert_one(comment.to_dict())
        return comment

from dbObjects import User, Post, Comment
