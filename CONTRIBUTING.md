# Soundbites
**A social app for sharing music** *- Group 13 - Integrated Group Project (CM20257)*

## Contributing
If you're not familiar with how [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/) works, here's a brief overview.

1. If you've not already done so, clone the repository. *(Use the Clone in Desktop button if you're feeling lazy)*
2. Checkout (switch to) the `develop` branch *(and preferably pull/sync whilst you're at it)*.
3. Create a new branch from `develop`, with a name in the format of `feature/branch-name` *(where `branch-name` describes the feature that you're implementing)*.
4. Do all the development work you need to do, making as many commits to the new feature branch as you need.
5. When done, publish/push the branch (if you've not done so already). Then, make a new pull request from your branch to `develop`.
6. Once enough people have approved your pull request, hit the merge button! If there are any requests for changes, add new commits to the branch, and then push it again.

I'm sure there are also ways of integrating this process with issues and projects, we'll see how they work as we go.

*Also, before you start contributing, make sure to set your [name](https://help.github.com/articles/setting-your-username-in-git/) and [email address](https://help.github.com/articles/setting-your-email-in-git/) in Git ;) (or GitHub Desktop or whatever)*

## Authors
- Liam Berrisford
- João Duarte
- Ben Hetherington
- Matthew Hewitt
- Adam Lassiter
- Venislav Venkov
