package com.projectvuvuzela.soundbites;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

/**
 * A custom subclass of Application, enabling us to get the application context from anywhere.
 */
public class App extends Application {
    private static App instance = new App();
    private Context context;

    public void onCreate() {
        super.onCreate();
        setContext(getApplicationContext());

        // Force all activities in this app to be portrait
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            // Unused
            public void onActivityDestroyed(Activity activity) { }
            public void onActivityResumed(Activity activity) { }
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) { }
            public void onActivityStopped(Activity activity) { }
            public void onActivityPaused(Activity activity) { }
            public void onActivityStarted(Activity activity) { }
        });

    }

    public static App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return getInstance().context;
    }

    public void setContext(Context context) {
        getInstance().context = context;
    }

}
