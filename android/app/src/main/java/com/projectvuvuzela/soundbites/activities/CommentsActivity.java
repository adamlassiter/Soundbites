package com.projectvuvuzela.soundbites.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.projectvuvuzela.soundbites.R;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.CommentData;
import com.projectvuvuzela.soundbites.networking.data.PostData;
import com.projectvuvuzela.soundbites.views.CommentView;
import com.projectvuvuzela.soundbites.views.SoundbiteView;

import static android.view.View.*;

public class CommentsActivity extends AppCompatActivity {

    final static public String POST_KEY = "post";

    public String postID;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        PostData postData = (PostData)getIntent().getSerializableExtra(POST_KEY);
        postID = postData.postID;
        getSoundbite().importData(postData);
        getSoundbite().showComments(false);

        updateComments();
    }

    private void updateComments() {
        getLoadingIndicator().setVisibility(VISIBLE);

        NetworkHandler.comment.get(postID, new ResponseHandler<CommentData[]>() {
            public void onResponse(boolean success, CommentData[] response, String error) {
                getLoadingIndicator().setVisibility(GONE);

                if (success) {
                    getNoCommentsText().setVisibility(response.length == 0 ? VISIBLE : GONE);

                    for (final CommentData comment : response) {
                        CommentView commentView = new CommentView(getBaseContext());
                        commentView.setUsername(comment.username);
                        commentView.setCommentText(comment.comment);

                        commentView.setUsernameTapHandler(new OnClickListener() {
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                                intent.putExtra(ProfilePage.USER_ID_KEY, comment.userID);
                                intent.putExtra(ProfilePage.USER_NAME_KEY, comment.username);
                                startActivity(intent);
                            }
                        });

                        getCommentThread().addView(commentView);
                    }

                } else {
                    Snackbar.make(getCommentThread(), error, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    public void addComment(final View view) {
        view.setEnabled(false);
        getAddCommentText().setEnabled(false);

        NetworkHandler.comment.create(postID, getAddCommentText().getText().toString(), new ResponseHandler<CommentData>() {
            public void onResponse(boolean success, CommentData response, String error) {
                view.setEnabled(true);
                getAddCommentText().setEnabled(true);

                if (success) {
                    getAddCommentText().setText("");
                    getNoCommentsText().setVisibility(GONE);

                    CommentView commentView = new CommentView(getBaseContext());
                    commentView.setUsername(NetworkHandler.getUserName());
                    commentView.setCommentText(response.comment);
                    getCommentThread().addView(commentView, 0);

                } else {
                    Snackbar.make(view, error, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private SoundbiteView getSoundbite() {
        return (SoundbiteView)findViewById(R.id.commentSoundbite);
    }

    private EditText getAddCommentText() {
        return (EditText)findViewById(R.id.addCommentText);
    }

    private Button getAddCommentButton() {
        return (Button)findViewById(R.id.addCommentButton);
    }

    private LinearLayout getCommentThread() {
        return (LinearLayout)findViewById(R.id.commentThread);
    }

    private ProgressBar getLoadingIndicator() {
        return (ProgressBar)findViewById(R.id.commentsLoading);
    }

    private TextView getNoCommentsText() {
        return (TextView)findViewById(R.id.noCommentsText);
    }

}
