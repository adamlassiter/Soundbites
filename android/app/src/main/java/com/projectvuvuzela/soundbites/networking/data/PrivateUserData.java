package com.projectvuvuzela.soundbites.networking.data;

import com.google.gson.annotations.SerializedName;

public class PrivateUserData extends UserData {

    @SerializedName("passwd")  public String password  = "";
    @SerializedName("salt")    public String salt      = "";
    @SerializedName("auth_tk") public String authToken = "";

}
