#!/usr/bin/python3

from flask import request, make_response
from flask_restful import Resource

from dbInterface import CommentDB, PostDB, UserDB
db = CommentDB()
post_db = PostDB()
user_db = UserDB()


def headers(*args):
    return request.headers.get(*args)


class CommentRequestHandler(Resource):

    # Get all comments on a given post
    def get(self):
        post_id = headers('post_id')
        comment_id = headers('comment_id')

        if post_id is None and comment_id is None:
            return make_response('Not enough arguments', 400)

        if post_id:
            post = post_db.get_post(post_id)

            if post:
                comments = map(lambda x: db.get_comment(x).to_dict(), post.comment_ids)
                return list(sorted(comments, key=lambda x: x['time'], reverse=True))
            else:
                return make_response('Post not found', 404)
        else:
            comment = db.get_comment(comment_id)

            if comment:
                return comment.to_dict()
            else:
                return make_response('Comment not found', 404)

    # Create a new comment on an existing post
    def post(self):
        post_id = headers('post_id')
        user_id = headers('user_id')
        comment_txt = headers('comment')
        auth_tk = headers('auth_tk')

        if None in [post_id, user_id, comment_txt, auth_tk]:
            return make_response('Not enough arguments', 400)

        post = post_db.get_post(post_id)
        user = user_db.get_user(user_id)
        comment = db.create_comment(post, user, comment_txt, auth_tk)
        return comment.to_dict()

    def put(self):
        pass

    def delete(self):
        pass
