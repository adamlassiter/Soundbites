package com.projectvuvuzela.soundbites.activities;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.projectvuvuzela.soundbites.App;
import com.projectvuvuzela.soundbites.R;

import android.widget.SeekBar;
import android.widget.TextView;


public class TrimmingPage extends AppCompatActivity {

    final static public String START_TIME_KEY = "startTime";
    final static public String END_TIME_KEY = "endTime";
    final static private int MAX_LENGTH = 30000; // 30 sec, in ms
    private Uri path;
    private int audioLength;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimming_page);
        setResult(RESULT_CANCELED);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        path = getIntent().getData();

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(App.getContext(), path);
        audioLength = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        mmr.release();

        getTrimStart().setMax(audioLength);
        getTrimEnd().setMax(audioLength);

        getTrimEnd().setProgress(Math.max(audioLength - MAX_LENGTH, 0));

        getTrimStart().setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if ((audioLength - getTrimEnd().getProgress()) - progress > MAX_LENGTH) {
                        // If the difference is too great between start and end
                        getTrimEnd().setProgress(audioLength - (progress + MAX_LENGTH));

                    } else if (progress > (audioLength - getTrimEnd().getProgress())) {
                        // If start is beyond the end
                        getTrimEnd().setProgress(audioLength - progress);
                    }
                }
                setTrimText();
            }

            // Unused
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        getTrimEnd().setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if ((audioLength - progress) - getTrimStart().getProgress() > MAX_LENGTH) {
                        // If the difference is too great between start and end
                        getTrimStart().setProgress(audioLength - (progress + MAX_LENGTH));

                    } else if (getTrimStart().getProgress() > (audioLength - progress)) {
                        // If start is beyond the end
                        getTrimStart().setProgress(audioLength - progress);
                    }
                }
                setTrimText();
            }

            // Unused
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        setTrimText();
    }

    public void performTrim(View view) {
        Intent intent = new Intent();
        intent.setData(path);
        intent.putExtra(START_TIME_KEY, getTrimStart().getProgress());
        intent.putExtra(END_TIME_KEY, audioLength - getTrimEnd().getProgress());
        setResult(RESULT_OK, intent);
        finish();
    }

    public SeekBar getTrimStart() {
        return (SeekBar)findViewById(R.id.seekBar);
    }

    public SeekBar getTrimEnd() {
        return (SeekBar)findViewById(R.id.seekBar2);
    }

    public void setTrimText() {
        TextView startLabel = (TextView)findViewById(R.id.TrimFront);
        TextView endLabel = (TextView)findViewById(R.id.TrimEnd);

        int startTime = getTrimStart().getProgress();
        int endTime = audioLength - getTrimEnd().getProgress();

        startLabel.setText(String.format("Start from %02d:%02d", startTime / 60000, (startTime % 60000) / 1000));
        endLabel.setText(String.format("End at %02d:%02d", endTime / 60000, (endTime % 60000) / 1000));
    }

}
