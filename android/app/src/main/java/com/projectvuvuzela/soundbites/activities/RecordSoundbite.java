package com.projectvuvuzela.soundbites.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.projectvuvuzela.soundbites.R;

import java.io.IOException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class RecordSoundbite extends AppCompatActivity {

    private MediaRecorder recorder = new MediaRecorder();
    private boolean recording = false;
    private Timer progressUpdateTimer;
    private int currentTime = 0;

    private String filename;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_soundbite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        filename = getCacheDir().getAbsolutePath() + "recording.m4a";

        if (checkSelfPermission(permissions[0]) != PackageManager.PERMISSION_GRANTED) {
            getStartStopButton().setEnabled(false);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }

        setRecordingProgress(0);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getStartStopButton().setEnabled(true);
            }
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        recorder.release();
    }

    public void handleStartStopButton(View view) {
        if (!recording) {
            startRecording();
        } else {
            stopRecording();
        }
    }


    private void startRecording() {
        try {
            startAudioRecording();
            getStartStopButton().setText(R.string.stop_recording);

            currentTime = 0;

            TimerTask task = new TimerTask() {
                public void run() {
                    currentTime++;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            RecordSoundbite.this.setRecordingProgress(currentTime);
                        }
                    });
                }
            };

            if (progressUpdateTimer != null) {
                progressUpdateTimer.cancel();
            }
            progressUpdateTimer = new Timer();
            progressUpdateTimer.scheduleAtFixedRate(task, 100, 100);

        } catch (Exception e) {
            // Catches more than just IOExceptions, as running this on an emulator causes a RuntimeException.
            Snackbar.make(getStartStopButton(), "Couldn't start recording: " + e.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
        }
    }

    private void stopRecording() {
        stopAudioRecording();
        getStartStopButton().setText(R.string.start_recording);
        progressUpdateTimer.cancel();
        Intent intent = new Intent(this, UploadActivity.class);
        intent.setData(Uri.parse(filename));
        startActivity(intent);
    }

    private void startAudioRecording() throws IOException {

        recorder.reset();
        recorder.setAudioSource(MediaRecorder.AudioSource.UNPROCESSED);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(filename);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        recorder.setMaxDuration(30 * 1000);

        recorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            public void onError(MediaRecorder mr, int what, int extra) {
                System.out.println("On Error: what: " + what + " - extra: " + extra);
            }
        });

        recorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            public void onInfo(MediaRecorder mr, int what, int extra) {
                System.out.println("On Info: what: " + what + " - extra: " + extra);
            }
        });

        recorder.prepare();
        recorder.start();
        recording = true;
    }

    private void stopAudioRecording() {
        recorder.stop();
        recording = false;
    }


    private void setRecordingProgress(int deciseconds) {
        TextView timeElapsed = (TextView) findViewById(R.id.timeElapsed);
        TextView timeRemaining = (TextView) findViewById(R.id.timeRemaining);
        ProgressBar recordingProgress = (ProgressBar) findViewById(R.id.recordingProgress);

        deciseconds = Math.max(Math.min(deciseconds, 300), 0);

        timeElapsed.setText(String.format(Locale.getDefault(), "0:%02d", deciseconds / 10));
        timeRemaining.setText(String.format(Locale.getDefault(), "-0:%02d", 30 - (deciseconds / 10)));
        recordingProgress.setProgress(deciseconds);
    }



    private Button getStartStopButton() {
        return (Button) findViewById(R.id.startStopButton);
    }

}
