package com.projectvuvuzela.soundbites.networking.data;

import com.google.gson.annotations.SerializedName;

public class LoginData {
    @SerializedName("_id")     public String userID    = "";
    @SerializedName("auth_tk") public String authToken = "";
}
