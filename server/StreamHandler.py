#!/usr/bin/python3

from flask import request, make_response
from flask_restful import Resource
from AudioManipulator import FileStream

from dbInterface import PostDB
db = PostDB()

def args(*args):
    return request.args.get(*args)


def get_soundbite(file_id):
    with FileStream(file_id) as stream:
        return stream.file.read()


class StreamHandler(Resource):
    
    # get the audio data, given a post id
    def get(self):
        post_id = args('post_id')
        
        if post_id is None:
            return make_response('Not enough arguments', 400)
        
        post = db.get_post(post_id)
        
        if post:
            return make_response(get_soundbite(post.file_nm))
        else:
            return make_response('Post not found', 404)
