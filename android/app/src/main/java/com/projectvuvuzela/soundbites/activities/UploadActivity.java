package com.projectvuvuzela.soundbites.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.projectvuvuzela.soundbites.R;
import com.projectvuvuzela.soundbites.Utils;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.data.PostData;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.projectvuvuzela.soundbites.activities.TrimmingPage.*;

public class UploadActivity extends AppCompatActivity {

    final static private int ACTIVITY_FILE_CHOOSER = 0;
    final static private int ACTIVITY_TRIM = 1;
    final static private int IMAGE_FILE_CHOOSER = 123;

    private Uri path;
    private Uri imgUri;
    private Integer startTime = null;
    private Integer endTime = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        path = getIntent().getData();

        ImageButton photoChooseButton = (ImageButton) findViewById(R.id.imageButton);
        photoChooseButton.setOnClickListener(choosePicturePressed);

        if (path == null) {
            Intent fileChooserIntent = new Intent(Intent.ACTION_GET_CONTENT);
            fileChooserIntent.setType("audio/*");
            startActivityForResult(fileChooserIntent, ACTIVITY_FILE_CHOOSER);
        }
    }

    public View.OnClickListener choosePicturePressed = new View.OnClickListener () {
        @Override
        public void onClick(View v) {
            Intent fileChooserIntent = new Intent(Intent.ACTION_GET_CONTENT);
            fileChooserIntent.setType("image/*");
            startActivityForResult(fileChooserIntent.createChooser(fileChooserIntent, "Select Image"), IMAGE_FILE_CHOOSER);
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case ACTIVITY_FILE_CHOOSER:
                getUploadButton().setEnabled(false);
                if (resultCode == RESULT_CANCELED) {
                    finish();
                    return;
                }
                Intent trimIntent = new Intent(getApplicationContext(), TrimmingPage.class);
                trimIntent.setData(intent.getData());
                startActivityForResult(trimIntent, ACTIVITY_TRIM);
                break;

            case ACTIVITY_TRIM:
                if (resultCode == RESULT_CANCELED) {
                    finish();
                    return;
                }

                path = intent.getData();
                startTime = intent.getExtras().getInt(START_TIME_KEY);
                endTime = intent.getExtras().getInt(END_TIME_KEY);
                getUploadButton().setEnabled(true);
                break;

            case IMAGE_FILE_CHOOSER:
                if (resultCode == RESULT_CANCELED) {
                    return;
                }

                imgUri = intent.getData();
                try {
                    ImageButton photoChooseButton = (ImageButton)findViewById(R.id.imageButton);
                    byte[] imgData = Utils.inputStreamToBytes(getContentResolver().openInputStream(imgUri));
                    Drawable image = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(imgData, 0, imgData.length));
                    photoChooseButton.setImageDrawable(image);
                    photoChooseButton.setScaleType(ImageView.ScaleType.CENTER_CROP);

                } catch (IOException e) {
                    // Don't worry too much yet
                    e.printStackTrace();
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, intent);
                break;
        }
    }

    protected void beginUpload(View view) {
        // First, hide the keyboard
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText() && getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        getUploadButton().setEnabled(false);
        getUploadButton().setText("Uploading…");

        try {
            byte[] data = Utils.inputStreamToBytes(getContentResolver().openInputStream(path));
            byte[] imgData = imgUri != null ? Utils.inputStreamToBytes(getContentResolver().openInputStream(imgUri)) : new byte[0];
            NetworkHandler.post.create(getTitleText().getText().toString(), data, ".m4a",getUploadDescription().getText().toString(), Base64.encodeToString(imgData,Base64.NO_WRAP), startTime, endTime, new NetworkHandler.ResponseHandler<PostData>() {
                public void onResponse(boolean success, PostData response, String error) {
                    if (success) {
                        finish();

                    } else {
                        Snackbar.make(getBaseLayout(), error, Snackbar.LENGTH_LONG).show();
                        getUploadButton().setEnabled(true);
                        getUploadButton().setText("Upload");
                        Log.d("ServerResponse",error);
                    }
                }
            });

        } catch (IOException e) {
            Snackbar.make(getBaseLayout(), e.getLocalizedMessage(), Snackbar.LENGTH_LONG);
        }
    }

    private Button getUploadButton() {
        return (Button)findViewById(R.id.uploadButton);
    }

    private EditText getTitleText() {
        return (EditText)findViewById(R.id.uploadTitle);
    }

    private EditText getUploadDescription () { return (EditText)findViewById(R.id.uploadDescription);}

    private View getBaseLayout() {
        return findViewById(R.id.uploadBaseLayout);
    }

}
