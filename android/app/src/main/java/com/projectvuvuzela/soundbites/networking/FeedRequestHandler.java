package com.projectvuvuzela.soundbites.networking;

import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.PostData;

import static com.android.volley.Request.Method.GET;
import static com.projectvuvuzela.soundbites.networking.NetworkHandler.BASE_URL;

/**
 * Interacts with the server's /feed API.
 * Access these methods like so: NetworkHandler.feed.getFeed(...)
 */
public class FeedRequestHandler {

    private NetworkHandler network;

    protected FeedRequestHandler(NetworkHandler networkHandler) {
        network = networkHandler;
    }

    public void getFriendFeed(ResponseHandler<PostData[]> handler) {
        getFriendFeed(NetworkHandler.getUserID(), NetworkHandler.getAuthToken(), handler);
    }

    public void getFriendFeed(String userID, String authToken, ResponseHandler<PostData[]> handler) {
        ObjectRequest request = new ObjectRequest<>(GET, BASE_URL + "/feed", PostData[].class, handler);
        request.putInHeader("user_id", userID);
        request.putInHeader("feed_type", "friends");
        request.putInHeader("auth_tk", authToken);

        network.queue.add(request);
    }

    public void getPersonalFeed(ResponseHandler<PostData[]> handler) {
        getPersonalFeed(NetworkHandler.getUserID(), handler);
    }

    public void getPersonalFeed(String userID, ResponseHandler<PostData[]> handler) {
        ObjectRequest request = new ObjectRequest<>(GET, BASE_URL + "/feed", PostData[].class, handler);
        request.putInHeader("user_id", userID);
        request.putInHeader("feed_type", "personal");

        network.queue.add(request);
    }

}
