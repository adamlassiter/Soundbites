package com.projectvuvuzela.soundbites.views;

import android.content.Context;
import android.support.v7.view.ContextThemeWrapper;
import android.util.AttributeSet;
import android.widget.*;

import com.projectvuvuzela.soundbites.R;


public class CommentView extends RelativeLayout {

    private Button usernameButton;
    private TextView commentText;

    public CommentView(Context context) {
        super(context);
        setUp();
    }

    public CommentView(Context context, AttributeSet attributes) {
        super(context, attributes);
        setUp();
    }

    public void setUp() {
        //noinspection RestrictedApi (lol whatevz)
        usernameButton = new Button(new ContextThemeWrapper(getContext(), R.style.Widget_AppCompat_Button_Borderless), null, 0);
        usernameButton.setId(generateViewId());
        addView(usernameButton);

        commentText = new TextView(getContext());
        commentText.setId(generateViewId());
        addView(commentText);

        LayoutParams usernameParams = (LayoutParams)usernameButton.getLayoutParams();
        usernameParams.addRule(ALIGN_PARENT_TOP);

        LayoutParams commentParams = (LayoutParams)commentText.getLayoutParams();
        commentParams.addRule(RIGHT_OF, usernameButton.getId());
        commentParams.addRule(CENTER_VERTICAL);

        setUsername("???");
        setCommentText("Loading…");
    }

    public void setUsername(CharSequence text) {
        usernameButton.setText(text);
    }

    public void setCommentText(CharSequence text) {
        commentText.setText(text);
    }

    public void setUsernameTapHandler(OnClickListener tapHandler) {
        usernameButton.setOnClickListener(tapHandler);
    }

}
