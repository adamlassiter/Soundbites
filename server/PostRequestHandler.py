#!/usr/bin/python3

from flask import request, make_response
from flask_restful import Resource

from AudioManipulator import AudioFile
from dbInterface import UserDB, PostDB, CommentDB
db = PostDB()
user_db = UserDB()
com_db = CommentDB()


def headers(*args):
    return request.headers.get(*args)


def data():
    return request.data


class PostRequestHandler(Resource):

    # get a post from the db given a post_id
    def get(self):
        post_id = headers('post_id')
        file_id = headers('file_id')

        if post_id is None and file_id is None:
            return make_response('Not enough arguments', 400)

        post = db.get_post(post_id)

        if post:
            return post.to_dict()
        else:
            return make_response('Post not found', 404)

    # create a new post by user_id if auth_tk is valid
    def post(self):
        user_id = headers('user_id')
        post_nm = headers('post_nm')
        auth_tk = headers('auth_tk')
        file_ex = headers('file_ex')
        start = headers('start_time')
        end = headers('end_time')
        img_art = headers('img_art')
        file_desc = headers('file_desc')
        soundbite = data()

        if None in [user_id, post_nm, auth_tk, file_ex, soundbite]:
            return make_response('Not enough arguments', 400)
            
        if (start == None) ^ (end == None):
            return make_response('Not enough arguments', 400)

        with AudioFile(file_ex, start, end) as file:
            file.write(soundbite)

        user = user_db.get_user(user_id)
        post = db.create_post(user, post_nm, file.file_nm, auth_tk, file_desc, img_art)
        return post.to_dict()

    # like a post
    def put(self):
        user_id = headers('user_id')
        post_id = headers('post_id')
        auth_tk = headers('auth_tk')

        if None in [user_id, post_id, auth_tk]:
            return make_response('Not enough arguments', 400)

        post = db.get_post(post_id)
        user = user_db.get_user(user_id)

        if post and user:
            return db.like_post(post, user, auth_tk).to_dict()
        else:
            return make_response('Post not found', 404)

    # delete an existing post by a user
    def delete(self):
        pass
