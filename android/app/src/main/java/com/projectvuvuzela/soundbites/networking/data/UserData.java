package com.projectvuvuzela.soundbites.networking.data;

import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("_id")        public String userID      = "";
    @SerializedName("user_nm")    public String username    = "";
    @SerializedName("post_ids")   public String[] postIDs   = {};
    @SerializedName("friend_ids") public String[] friendIDs = {};

}
