package com.projectvuvuzela.soundbites.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.ContextThemeWrapper;
import android.widget.ImageButton;

import com.projectvuvuzela.soundbites.R;


public class BorderlessImageButton extends ImageButton {

    public BorderlessImageButton(Context context, boolean dark) {
        // If "dark" (i.e. on black background), use a custom style with a light touch-down colour
        super(dark ? new ContextThemeWrapper(context, R.style.DarkColourControl) : context);

        // Use Material's button touch-down animation
        int[] attrs = new int[] {android.R.attr.selectableItemBackgroundBorderless};
        TypedArray styledAttrs = getContext().obtainStyledAttributes(attrs);
        Drawable selectableItemDrawable = styledAttrs.getDrawable(0);
        styledAttrs.recycle();
        setBackground(selectableItemDrawable);
    }

}
