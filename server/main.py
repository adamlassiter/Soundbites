#!/usr/bin/python3

from UserRequestHandler import UserRequestHandler
from FeedRequestHandler import FeedRequestHandler
from PostRequestHandler import PostRequestHandler
from CommentRequestHandler import CommentRequestHandler
from UserAuthenticator import UserAuthenticator
from StreamHandler import StreamHandler

from flask import Flask
from flask_restful import Api
from ssl import SSLContext, PROTOCOL_SSLv23

server = Flask(__name__)
api = Api(server)

# HACK: avoid using flask-restful's partial functions
server.handle_exception = server.handle_exception.args[0]
server.handle_user_exception = server.handle_user_exception.args[0]


# basic homepage, easily changeable
@server.route('/')
def index():
    return 'Soundbites'


# adherance to strict ISO standards
@server.route('/teapot')
def teapot():
    return 'teapot', 418


# exception handler
@server.errorhandler(Exception)
def exception_handler(e):
    Flask.log_exception(server, e)
    return str(e), 400


api.add_resource(UserRequestHandler, '/user',
                 methods=['GET', 'POST', 'PUT'])  # DELETE

api.add_resource(FeedRequestHandler, '/feed',
                 methods=['GET'])

api.add_resource(PostRequestHandler, '/post',
                 methods=['GET', 'POST', ])  # PUT, DELETE

api.add_resource(CommentRequestHandler, '/comment',
                 methods=['GET', 'POST'])  # PUT, DELETE

api.add_resource(UserAuthenticator, '/login',
                 methods=['PUT', 'DELETE'])
                 
api.add_resource(StreamHandler, '/stream',
                 methods=['GET'])


if __name__ == '__main__':
    from os import getenv
    context = SSLContext(PROTOCOL_SSLv23)
    context.load_cert_chain('server/ssl.cert', 'server/ssl.key')
    server.run(debug=False, ssl_context=context,
               host=getenv('IP', '0.0.0.0'), port=int(getenv('PO‌​RT', 5000)))
