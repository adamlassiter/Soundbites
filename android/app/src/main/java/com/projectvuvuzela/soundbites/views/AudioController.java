package com.projectvuvuzela.soundbites.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.*;

import com.projectvuvuzela.soundbites.Utils;
import com.projectvuvuzela.soundbites.R;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class AudioController extends RelativeLayout {

    private ImageButton playPauseButton;
    private ImageButton leftSeek;
    private ImageButton rightSeek;

    private TextView timeElapsed;
    private TextView timeRemaining;
    private SeekBar seekBar;

    private boolean paused;
    private int duration;

    private Timer updateTimer = new Timer();
    private CallbackHandler callback;

    public AudioController(Context context) {
        super(context);
        setUpViews(context);
        setUpLayout();
        setUpActions();
    }

    public AudioController(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpViews(context);
        setUpLayout();
        setUpActions();
    }

    private void setUpViews(Context context) {
        int padding = Utils.dpToPx(10, context);
        setPadding(padding, padding / 3, padding, padding);
        setBackgroundColor(getResources().getColor(R.color.controllerBackground, null));

        playPauseButton = new BorderlessImageButton(context, true);
        playPauseButton.setId(View.generateViewId());
        addView(playPauseButton);

        leftSeek = new BorderlessImageButton(context, true);
        leftSeek.setId(View.generateViewId());
        leftSeek.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_rew, null));
        addView(leftSeek);

        rightSeek = new BorderlessImageButton(context, true);
        rightSeek.setId(View.generateViewId());
        rightSeek.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_ff, null));
        addView(rightSeek);

        timeElapsed = new TextView(context);
        timeElapsed.setId(View.generateViewId());
        timeElapsed.setTextColor(Color.WHITE);
        addView(timeElapsed);

        timeRemaining = new TextView(context);
        timeRemaining.setId(View.generateViewId());
        timeRemaining.setTextColor(Color.WHITE);
        addView(timeRemaining);

        seekBar = new SeekBar(context);
        seekBar.setId(View.generateViewId());
        seekBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.controllerSeekBarForeground, null)));
        seekBar.setThumbTintList(ColorStateList.valueOf(getResources().getColor(R.color.controllerSeekBarForeground, null)));
        seekBar.setSecondaryProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.controllerSeekBarBuffered, null)));
        seekBar.setProgressBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.controllerSeekBarBackground, null)));
        addView(seekBar);

        setEnabled(false);
        setPlaying(false);
        setProgress(0);
        setDuration(0);
        setBuffer(0);
    }

    private void setUpLayout() {
        LayoutParams playPauseLayout = (LayoutParams)playPauseButton.getLayoutParams();
        playPauseLayout.addRule(CENTER_HORIZONTAL);
        int padding = Utils.dpToPx(15, getContext());
        playPauseLayout.setMargins(padding, 0, padding, 0);

        LayoutParams leftSeekLayout = (LayoutParams)leftSeek.getLayoutParams();
        leftSeekLayout.addRule(LEFT_OF, playPauseButton.getId());

        LayoutParams rightSeekLayout = (LayoutParams)rightSeek.getLayoutParams();
        rightSeekLayout.addRule(RIGHT_OF, playPauseButton.getId());

        LayoutParams seekBarLayout = (LayoutParams)seekBar.getLayoutParams();
        seekBarLayout.addRule(BELOW, playPauseButton.getId());
        seekBarLayout.addRule(CENTER_HORIZONTAL);
        seekBarLayout.addRule(RIGHT_OF, timeElapsed.getId());
        seekBarLayout.addRule(LEFT_OF, timeRemaining.getId());

        LayoutParams timeElaspedLayout = (LayoutParams)timeElapsed.getLayoutParams();
        timeElaspedLayout.addRule(BELOW, playPauseButton.getId());
        timeElaspedLayout.addRule(ALIGN_PARENT_LEFT);

        LayoutParams timeRemainingLayout = (LayoutParams)timeRemaining.getLayoutParams();
        timeRemainingLayout.addRule(BELOW, playPauseButton.getId());
        timeRemainingLayout.addRule(ALIGN_PARENT_RIGHT);
    }

    private void setUpActions() {
        playPauseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (paused) {
                    callback.start();
                } else {
                    callback.pause();
                }

                setPlaying(paused); // This will invert the now playing state
            }
        });

        leftSeek.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                callback.seekTo(Math.max(seekBar.getProgress() - 5000, 0));
            }
        });

        rightSeek.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                callback.seekTo(Math.min(seekBar.getProgress() + 5000, seekBar.getMax()));
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    callback.seekTo(progress);
                    setProgress(progress);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!paused) {
                    callback.pause();
                }
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!paused) {
                    callback.start();
                }
            }
        });
    }

    public void setProgress(final int elapsed) {
        final int elapsedSec = elapsed / 1000;
        final int maxSec = duration / 1000;
        final int remainingSec = maxSec - elapsedSec;

        Handler mainThreadHandler = new Handler(getContext().getMainLooper());
        mainThreadHandler.post(new Runnable() {
            public void run() {
                timeElapsed.setText(String.format(Locale.getDefault(), "%01d:%02d", (elapsedSec / 60), (elapsedSec % 60)));
                timeRemaining.setText(String.format(Locale.getDefault(), "-%01d:%02d", (remainingSec / 60), (remainingSec % 60)));

                seekBar.setProgress(elapsed);
            }
        });
    }

    public void setDuration(int uncappedMax) {
        duration = Math.max(uncappedMax, 1);
        seekBar.setMax(duration);
    }

    public void setCallbackHandler(CallbackHandler callback) {
        this.callback = callback;
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        playPauseButton.setEnabled(enabled);
        leftSeek.setEnabled(enabled);
        rightSeek.setEnabled(enabled);
        seekBar.setEnabled(enabled);

        int buttonAlpha = enabled ? 255 : 127;
        playPauseButton.setImageAlpha(buttonAlpha);
        leftSeek.setImageAlpha(buttonAlpha);
        rightSeek.setImageAlpha(buttonAlpha);
    }

    public void setBuffer(int percentage) {
        seekBar.setSecondaryProgress((percentage * seekBar.getMax()) / 100);
    }

    public void setPlaying(boolean playing) {
        updateTimer.cancel();
        paused = !playing;

        if (playing) {
            playPauseButton.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_pause, null));
            updateTimer = new Timer();
            updateTimer.scheduleAtFixedRate(new UpdateTask(), 0, 100);

        } else {
            playPauseButton.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play, null));
        }
    }

    private class UpdateTask extends TimerTask {
        public void run() {
            if (isEnabled()) {
                setProgress(callback.getPosition());
            }
        }
    }

    public interface CallbackHandler {
        void start();
        void pause();
        void seekTo(int position);
        int getPosition();
    }

}
