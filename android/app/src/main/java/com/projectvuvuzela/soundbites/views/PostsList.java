package com.projectvuvuzela.soundbites.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.projectvuvuzela.soundbites.Utils;
import com.projectvuvuzela.soundbites.R;

public class PostsList extends LinearLayout {

    private ProgressBar loadingIndicator;
    private Button loadMoreButton;

    private int postPadding;

    public PostsList(Context context) {
        super(context);
        setUp(context);
    }

    public PostsList(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setUp(context);
    }

    protected void setUp(Context context) {
        loadingIndicator = new ProgressBar(context);
        super.addView(loadingIndicator, 0); // Using this method to avoid setting the larger margins

        loadMoreButton = new Button(context);
        loadMoreButton.setText(R.string.load_more_button);
        super.addView(loadMoreButton, 0);

        showLoadingIndicator(false);

        postPadding = Utils.dpToPx(10, getContext());
    }

    public void showLoadingIndicator(boolean show) {
        if (show) {
            loadingIndicator.setVisibility(VISIBLE);
            loadMoreButton.setVisibility(GONE);
        } else {
            loadingIndicator.setVisibility(GONE);
            loadMoreButton.setVisibility(VISIBLE);
        }
    }

    public void addView(View child) {
        addView(child, getChildCount() - 2);
    }

    public void addView(View child, int index) {
        super.addView(child, index);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) child.getLayoutParams();
        params.setMargins(0, 0, 0, postPadding);
    }

    public Button getLoadMoreButton() {
        return loadMoreButton;
    }

    public void setLoadMoreListener(OnClickListener listener) {
        loadMoreButton.setOnClickListener(listener);
    }

}
