# Soundbites
**A social app for sharing music** _- Group 13 - Integrated Group Project (CM20257)_

## Server API
The server API is divided into three sections, each found at their appropriate page.
_e.g._ `localhost:5000/*User* -> User API`

Calls to the API are all made through passing data (JSON objects) to the HTML header.
The structure of each request is described below.
All types are *string* unless specified otherwise.
All headers requiring 'auth_tk' AuthTokens will return 401 errors for invalid tokens.



### User ( _/user_ )
Used for making requests about *Users*, this supports **GET**, **POST**, **PUT**, ~~DELETE~~.

#### GET
`Header = {'user_nm' | 'user_id'}`  
`Return = User {'_id', 'user_nm', 'post_ids' : list<_id>, 'friend_ids' : list<_id>}`  
Look-up a *User* given either a username __user_nm__ or userID ___id__.
Returns some data representing the stored *User* and the __post_ids__ of all their past posts, oldest to newest, or None if *User* does not exist.

#### POST
`Header = {'user_nm', 'passwd'}`  
`Return = User {... , 'auth_tk'}`  
Create a new *User* with a desired username __user_nm__ and (unhashed) password __passwd__.
Returns the data representing the stored User, or 303 Error if a *User* with that name already exists.

#### PUT
`Header = {'user_id', 'friend_id', 'auth_tk'}`
`Return = User {...}`
Add a friend by userID __friend_id__ 



### Feed ( _/feed_ )
Used for getting a given *User*'s feed or a collection of their friends' feeds, this supports **GET**.

#### GET
`Header = {'user_id', 'feed_type', 'auth_tk'}`  
`Return = {list<Post {...}>}`  
Look-up a __user_id__ *User*'s feed, specifying the feed type __feed_type__ to be either 'personal' or 'friends' for the respective feeds.
Returns a list of *Post*s, sorted in chronological order from newest to oldest.



### Post ( _/post_ )
Used for making requests about posts by *Users*, this supports **GET**, **POST**, **PUT**, ~~DELETE~~.

#### GET
`Header = {'post_id'}`  
`Return = Post {'_id', 'user_id', 'user_nm', 'post_nm', 'comment_ids' : list<_id>, 'file_nm', 'data', 'time' : double}`  
Look-up a post's content given the __post_id__.
Returns the data representing the stored *Post*, or None if post does not exist, where __file_nm__ is the name of the soundbite file as it is stored on the server, and __data__ the binary data of the file.

#### POST
`Header = {'user_id', 'post_nm', 'auth_tk', 'file_ex', 'img_art', 'file_desc', 'data'}`  
`Return = Post {...}`  
Create a new post with a desired post title __post_nm__ and __soundbite__ file, with specified file extension __file_ex__. It should be noted that the __soundbite__ file is posted to the HTTP __Data__ request, and not the Header.
Returns the data representing the stored *Post*.

#### PUT
`Header = {'post_id', 'user_id', 'autk_tk'}`
`Return = Post {...}`
Like a given post __post_id__ as *User* __user_id__. It should be noticed that we are wholesome people and there is currently no ability to 'un-like'.  
Returns the data representing the stored *Post*.  



### Stream ( _/stream_ )
Used to retrieve the audio data for a *Post*, this supports **GET**.

#### GET
`Header = {'post_id'}`
`Return = <audio data>`
Returns the audio data for the given *Post*.



### Comment( _/comment_ )
Used for making requests about *Comment*s on *Posts*, this supports **GET**, **POST**, ~~PUT~~, ~~DELETE~~.

#### GET
`Header = {'comment_id' | 'post_id'}`  
`Return = list<Comment {'_id', 'user_id', 'user_nm', 'post_id', 'comment'}> | Comment{...}`  
Look-up all *Comment*s on a given post __post_id__, or just a specific comment __comment_id__.
Returns the __user_id__ and __user_nm__ of the poster, and the __post_id__ of the post it was made on.

#### POST
`Header = {'post_id', 'user_id', 'comment', 'auth_tk'}`  
`Return = Comment {...}`  
Create a new *Comment* of __comment__ on the post __post_id__ as *User* __user_id__.



### Login ( _/login_ )
Used for 'logging in' as a given *User*, this supports **PUT**, **DELETE**.

#### PUT
`Header = {'user_nm', 'passwd'}`  
`Return = {'_id', 'auth_tk'}`  
Log-in a *User* and create an __auth_tk__ AuthToken for them. This token should be expired once no-longer in use.

#### DELETE
`Header = {'user_id', 'auth_tk'}`  
`Return = {null}`  
Log-out a *User* and expire their current __auth_tk__ AuthToken.
