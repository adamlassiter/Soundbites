package com.projectvuvuzela.soundbites.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.projectvuvuzela.soundbites.R;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.PrivateUserData;

public class SignUp extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }

    public void signUp(final View view) {
        String username = getUsername().getText().toString();
        String password = getPassword().getText().toString();
        String passwordRepeat = getPasswordRepeat().getText().toString();

        if (!password.equals(passwordRepeat)) {
            Snackbar.make(view, "The entered passwords do not match.", Snackbar.LENGTH_LONG).show();
            return;
        }

        view.setEnabled(false);

        NetworkHandler.user.create(username, password, new ResponseHandler<PrivateUserData>() {
            public void onResponse(boolean success, PrivateUserData response, String error) {
                if (success) {
                    startActivity(new Intent(SignUp.this, NavigationActivity.class));
                    sendBroadcast(new Intent("finish_login_activity"));
                    finish();

                } else {
                    Snackbar.make(view, "Couldn't create account: " + error, Snackbar.LENGTH_LONG).show();
                }

                view.setEnabled(true);
            }
        });
    }

    protected EditText getUsername() {
        return (EditText)findViewById(R.id.createUsername);
    }

    protected EditText getPassword() {
        return (EditText)findViewById(R.id.createPassword);
    }

    protected EditText getPasswordRepeat() {
        return (EditText)findViewById(R.id.createPasswordRepeat);
    }

}
