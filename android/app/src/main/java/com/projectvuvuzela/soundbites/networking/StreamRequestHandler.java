package com.projectvuvuzela.soundbites.networking;

/**
 * Interacts with the server's /post API.
 * Access these methods like so: NetworkHandler.post.getContent(...)
 */
public class StreamRequestHandler {

    private NetworkHandler network;

    protected StreamRequestHandler(NetworkHandler networkHandler) {
        network = networkHandler;
    }

    public String getStreamURL(String postID) {
        return NetworkHandler.BASE_URL + "/stream?post_id=" + postID;
    }

}
