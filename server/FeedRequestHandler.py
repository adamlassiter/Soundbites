#!/usr/bin/python3

from flask import request, make_response
from flask_restful import Resource
import functools

from dbInterface import UserDB, PostDB
user_db = UserDB()
post_db = PostDB()


def headers(*args):
    return request.headers.get(*args)


class FeedRequestHandler(Resource):

    # get a user's feed given a user_id
    # this may be their personal feed or friend feed
    def get(self):
        user_id = headers('user_id')
        feed_type = headers('feed_type')
        auth_tk = headers('auth_tk')

        if None in [user_id, feed_type]:
            return make_response('Not enough arguments', 400)

        user = user_db.get_user(user_id)

        if user:
            if feed_type == 'personal':
                post_ids = user.post_ids

            elif feed_type == 'friends':
                if user.auth_tk != auth_tk:
                    return make_response('Access denied', 401)
                
                friends = user_db.get_friends(user)
                friends_posts_ids = map(lambda f: f.post_ids, friends)
                post_ids = functools.reduce(lambda x, y: x + y, friends_posts_ids, [])
                post_ids.extend(user.post_ids)
            else:
                return make_response('Invalid arguments', 400)

            posts = filter(lambda x: x != None, map(post_db.get_post, post_ids))
            sorted_posts = sorted(posts, key=lambda x: x.time, reverse=True)
            return list(map(lambda x: x.to_dict(), sorted_posts))

        else:
            return make_response('Invalid arguments', 400)
