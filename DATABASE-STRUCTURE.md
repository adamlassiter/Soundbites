# Soundbites
**A social app for sharing music** _- Group 13 - Integrated Group Project (CM20257)_

## Database Structure
The server API uses a MongoDB database backend, with a layout as described herein.  
Variable types are all *string* unless specified otherwise.

## Objects Contained in Database
The database system stores three different classes of object, each of which is stored in their respective databases.


### User
`User = { 'user_id', 'user_nm', 'passwd', 'salt', 'auth_tk', 'posts' : list<str> }`  
A __User__ object is a collection of these details about the user, including a list of post objects that they have created.

`user_id` : A unique identifier of the user, a UUID  
`user_nm` : The username of the user, which may be displayed publicly  
`passwd`  : The sha256 hash of the user's password when combined with their salt  
`salt`    : A 32-char string of random (printable) characters
`auth_tk` : A UUID authorisation token - this should be empty if the user is logged out  
`posts`   : A list of __Post__ UUIDs created by the user  


### Post
`Post = { 'post_id', 'user_id', 'post_nm', 'file_nm', 'comments' : list<str> ] }`  
A __Post__ object is a collection of these details about a post made by it's parent user, including a list of comments that have been made on the post.

`post_id`  : A unique identifier of the post, a UUID
`user_id`  : The user_id of the user who created the post, a UUID  
`post_nm`  : The title of the post, which may be displayed publicly  
`flie_nm`  : The name of the file containing the soundbite of the post, as named and stored on the server  
`comments` : A list of __Comment__ UUIDs created by other users


### Comment
`Comment = { 'comment_id', 'post_id', 'user_id', 'comment' }`  
A __Comment__ is a pair of the comment's poster user_id and the comment content, as well as a reference to the parent post.

`comment_id` : A unique identifier of the comment, a UUID  
`post_id`    : The post_id of the post the comment is made upon, a UUID  
`user_id`    : The user_id of the user who created the comment  , a UUID  
`comment`    : The text content of the comment
