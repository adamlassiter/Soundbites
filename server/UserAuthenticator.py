#!/usr/bin/python3

from flask import request, make_response
from flask_restful import Resource

from dbInterface import UserDB
db = UserDB()


def headers(*args):
    return request.headers.get(*args)


class UserAuthenticator(Resource):

    # given a username and password, return an authentication token
    def put(self):
        user_nm, passwd = headers('user_nm'), headers('passwd')

        if None in [user_nm, passwd]:
            return make_response('Not enough arguments', 400)

        user = db.get_user_by_name(user_nm)

        if user:
            if user.verify_password(passwd):
                return {'_id': user.user_id,
                        'auth_tk': db.set_auth_token(user)}, 200
            else:
                return make_response('Access denied', 401)
        else:
            return make_response('User not found', 404)

    def delete(self):
        user_id, auth_tk = headers('user_id'), headers('auth_tk')

        if None in [user_id, auth_tk]:
            return make_response('Not enough arguments', 400)

        user = db.get_user(user_id)

        if user.auth_tk == auth_tk:
            db.unset_auth_token(user)
            return 'Success'
        else:
            return make_response('Access denied', 401)
