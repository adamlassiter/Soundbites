package com.projectvuvuzela.soundbites.networking;

import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.CommentData;

import static com.android.volley.Request.Method.*;
import static com.projectvuvuzela.soundbites.networking.NetworkHandler.BASE_URL;

/**
 * Interacts with the server's /comment API.
 * Access these methods like so: NetworkHandler.comment.get(...)
 */
public class CommentRequestHandler {

    private NetworkHandler network;

    protected CommentRequestHandler(NetworkHandler networkHandler) {
        network = networkHandler;
    }

    public void get(String postID, ResponseHandler<CommentData[]> handler) {
        ObjectRequest request = new ObjectRequest<>(GET, BASE_URL + "/comment", CommentData[].class, handler);
        request.putInHeader("post_id", postID);

        network.queue.add(request);
    }

    public void create(String postID, String comment, ResponseHandler<CommentData> handler) {
        create(postID, NetworkHandler.getUserID(), comment, NetworkHandler.getAuthToken(), handler);
    }

    public void create(String postID, String userID, String comment, String authToken, ResponseHandler<CommentData> handler) {
        ObjectRequest request = new ObjectRequest<>(POST, BASE_URL + "/comment", CommentData.class, handler);
        request.putInHeader("post_id", postID);
        request.putInHeader("user_id", userID);
        request.putInHeader("comment", comment);
        request.putInHeader("auth_tk", authToken);

        network.queue.add(request);
    }

}
