package com.projectvuvuzela.soundbites.networking;

import android.util.Log;

import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.*;
import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;

import java.io.UnsupportedEncodingException;
import java.util.*;

class ObjectRequest<T> extends Request<T> {

    final private Gson gson = new Gson();
    final private Class<T> theClass;
    final private ResponseHandler<T> handler;
    private Map<String, String> headers = new HashMap<>();
    private byte[] bodyData = null;

    public ObjectRequest(int method, String url, byte[] bodyData, Class<T> theClass, ResponseHandler<T> handler) {
        super(method, url, new ErrorParser<>(handler));

        this.theClass = theClass;
        this.handler = handler;
        this.bodyData = bodyData;
    }

    public ObjectRequest(int method, String url, Class<T> theClass, ResponseHandler<T> handler) {
        super(method, url, new ErrorParser<>(handler));

        this.theClass = theClass;
        this.handler = handler;
    }

    protected void deliverResponse(T response) {
        handler.onResponse(true, response, null);
    }

    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.d("DEBUG ALREADY EXISTS", json);
            return Response.success(gson.fromJson(json, theClass), HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException | JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void putInHeader(String key, String value) {
        headers.put(key, value);
    }

    public byte[] getBody() throws AuthFailureError {
        return bodyData;
    }

}
