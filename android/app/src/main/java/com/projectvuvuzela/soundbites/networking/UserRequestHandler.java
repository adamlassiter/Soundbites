package com.projectvuvuzela.soundbites.networking;

import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.*;

import static com.android.volley.Request.Method.*;
import static com.projectvuvuzela.soundbites.networking.NetworkHandler.*;

/**
 * Interacts with the server's /user API.
 * Access these methods like so: NetworkHandler.user.getPostsForUsername(...)
 */
public class UserRequestHandler {

    private NetworkHandler network;

    protected UserRequestHandler(NetworkHandler networkHandler) {
        network = networkHandler;
    }

    public void getPostsForUsername(String username, ResponseHandler<UserData> handler) {
        ObjectRequest request = new ObjectRequest<>(GET, BASE_URL + "/user", UserData.class, handler);
        request.putInHeader("user_nm", username);

        network.queue.add(request);
    }

    public void getPostsForUserID(String userID, ResponseHandler<UserData> handler) {
        ObjectRequest request = new ObjectRequest<>(GET, BASE_URL + "/user", UserData.class, handler);
        request.putInHeader("user_id", userID);

        network.queue.add(request);
    }

    public void create(String username, String password, ResponseHandler<PrivateUserData> handler) {
        create(username, password, true, handler);
    }

    public void create(String username, String password, boolean handleResponse, ResponseHandler<PrivateUserData> handler) {
        if (handleResponse) {
            final ResponseHandler<PrivateUserData> userHandler = handler;

            handler = new ResponseHandler<PrivateUserData>() {
                public void onResponse(boolean success, PrivateUserData response, String error) {
                    if (success) {
                        NetworkHandler.setUserID(response.userID, response.username, response.authToken);
                    }

                    userHandler.onResponse(success, response, error);
                }
            };
        }

        ObjectRequest request = new ObjectRequest<>(POST, BASE_URL + "/user", PrivateUserData.class, handler);
        request.putInHeader("user_nm", username);
        request.putInHeader("passwd", password);

        network.queue.add(request);
    }

    public void addFriend(String friendID, ResponseHandler<UserData> handler) {
        addFriend(getUserID(), friendID, getAuthToken(), handler);
    }

    public void addFriend(String userID, String friendID, String authToken, ResponseHandler<UserData> handler) {
        ObjectRequest request = new ObjectRequest<>(PUT, BASE_URL + "/user", UserData.class, handler);
        request.putInHeader("user_id", userID);
        request.putInHeader("friend_id", friendID);
        request.putInHeader("auth_tk", authToken);

        network.queue.add(request);
    }

}
