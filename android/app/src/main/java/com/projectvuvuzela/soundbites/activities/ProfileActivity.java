package com.projectvuvuzela.soundbites.activities;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import static android.view.View.generateViewId;

/**
 * A minimal activity, containing just the profile fragment.
 * Allows said fragment to be reused modally when looking at other user's profiles
 */
public class ProfileActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout layout = new FrameLayout(getApplicationContext());
        layout.setId(generateViewId());
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        ProfilePage fragment = new ProfilePage();
        fragment.setArguments(getIntent().getExtras());

        fragmentTransaction.add(layout.getId(), fragment);
        fragmentTransaction.commit();

        setContentView(layout);
    }

}
