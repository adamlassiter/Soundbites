package com.projectvuvuzela.soundbites.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.projectvuvuzela.soundbites.R;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.data.LoginData;

public class LoginActivity extends AppCompatActivity {

    BroadcastReceiver broadcastReciever;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (NetworkHandler.getAuthToken() != null) {
            // If we're already logged in, then what are we doing here?
            startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
            finish();

        } else {
            broadcastReciever = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals("finish_login_activity")) {
                        unregisterReceiver(broadcastReciever);
                        finish();
                    }
                }
            };

            registerReceiver(broadcastReciever, new IntentFilter("finish_login_activity"));
        }
    }

    public void onClickLogin(final View view) {
        view.setEnabled(false);
        String username = getUsername().getText().toString();
        String password = getPassword().getText().toString();

        NetworkHandler.login.login(username, password, new NetworkHandler.ResponseHandler<LoginData>() {
            public void onResponse(boolean success, LoginData response, String error) {
                if (success) {
                    startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
                    unregisterReceiver(broadcastReciever);
                    finish();

                } else {
                    Snackbar.make(view, "Couldn't log in: " + error, Snackbar.LENGTH_LONG).show();
                }

                view.setEnabled(true);
            }
        });
    }

    protected void goToSignup(View viewItem) {
        startActivity(new Intent(this, SignUp.class));
    }

    protected EditText getUsername() {
        return (EditText)findViewById(R.id.loginUsername);
    }

    protected EditText getPassword() {
        return (EditText)findViewById(R.id.loginPassword);
    }

}
