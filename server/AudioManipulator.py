#!/usr/bin/python3

from uuid import uuid1
from math import ceil
from os import path, remove
from subprocess import run


UPLOAD_FOLDER = 'server/uploads'
TEMP_FOLDER = 'server/tmp'
DEFAULT_FTYPE = '.m4a'
BUFFER_SIZE = 1 << 16


class AudioFile(object):

    def __init__(self, extension=DEFAULT_FTYPE, start=None, end=None):
        self.file_nm = str(uuid1()) + DEFAULT_FTYPE
        self.file_ext = DEFAULT_FTYPE
        self.tmp_ext = extension
        self.start = start
        self.end = end

    # Open a file object in TEMP_FOLDER
    def __enter__(self):
        self.tmp_path = path.join(TEMP_FOLDER, self.file_nm + self.tmp_ext)
        self.file = open(self.tmp_path, 'wb').__enter__()
        return self

    # Convert the file object to an AAC file if needed
    def __exit__(self, *args):
        self.file_path = path.join(UPLOAD_FOLDER, self.file_nm)
        if self.tmp_ext != DEFAULT_FTYPE or self.start or self.end:
            command = ['ffmpeg']
            if self.start:
                command.extend(['-ss', str(float(self.start) / 1000)])
            if self.start and self.end:
                command.extend(['-t', str((float(self.end) / 1000) - (float(self.start) / 1000))])
            command.extend(['-i', self.tmp_path])
            command.append(self.file_path)
            run(command)
            remove(self.tmp_path)
        else:
            run(['mv', self.tmp_path, self.file_path])
        return self.file.__exit__(*args)

    def write(self, *args):
        return self.file.write(*args)


class FileStream(object):

    def __init__(self, file_nm):
        self.file_nm = file_nm
        self.file_ext = DEFAULT_FTYPE

    def __iter__(self):
        return iter(partial(self.file.read, BUFFER_SIZE), '')

    def __enter__(self):
        fpath = path.join(UPLOAD_FOLDER, self.file_nm)
        self.file = open(fpath, 'rb').__enter__()
        return self

    def __exit__(self, *args):
        return self.file.__exit__(*args)
