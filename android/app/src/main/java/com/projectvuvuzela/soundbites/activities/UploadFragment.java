package com.projectvuvuzela.soundbites.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.projectvuvuzela.soundbites.R;

public class UploadFragment extends Fragment {

    private View rootView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_upload, container, false);

        // Setting onClick in the XML tries to find the methods in the parent activity, so let's do it the old-fashioned way
        getMicButton().setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getContext(), RecordSoundbite.class));
            }
        });


        getFileButton().setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getContext(), UploadActivity.class));
            }
        });

        return rootView;
    }

    private Button getMicButton() {
        return (Button)rootView.findViewById(R.id.uploadWithMic);
    }

    private Button getFileButton() {
        return (Button)rootView.findViewById(R.id.uploadFromFile);
    }

}
