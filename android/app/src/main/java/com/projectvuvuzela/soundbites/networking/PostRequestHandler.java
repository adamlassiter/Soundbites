package com.projectvuvuzela.soundbites.networking;

import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;
import com.projectvuvuzela.soundbites.networking.data.PostData;

import static com.android.volley.Request.Method.*;
import static com.projectvuvuzela.soundbites.networking.NetworkHandler.BASE_URL;

/**
 * Interacts with the server's /post API.
 * Access these methods like so: NetworkHandler.post.getContent(...)
 */
public class PostRequestHandler {

    private NetworkHandler network;

    protected PostRequestHandler(NetworkHandler networkHandler) {
        network = networkHandler;
    }

    public void getContent(String postID, ResponseHandler<PostData> handler) {
        ObjectRequest request = new ObjectRequest<>(GET, BASE_URL + "/post", PostData.class, handler);
        request.putInHeader("post_id", postID);

        network.queue.add(request);
    }

    public void create(String postName, byte[] audioData, String fileExtension, String file_desc,String img_art, Integer startTime, Integer endTime, ResponseHandler<PostData> handler) {
        create(NetworkHandler.getUserID(), postName, audioData, fileExtension, file_desc, img_art, startTime, endTime, NetworkHandler.getAuthToken(), handler);
    }

    public void create(String userID, String postName, byte[] audioData, String fileExtension, String file_desc, String img_art, Integer startTime, Integer endTime, String authToken, ResponseHandler<PostData> handler) {
        ObjectRequest request = new ObjectRequest<>(POST, BASE_URL + "/post", audioData, PostData.class, handler);
        request.putInHeader("Content-Type", "audio/aac");
        request.putInHeader("user_id", userID);
        request.putInHeader("post_nm", postName);
        request.putInHeader("auth_tk", authToken);
        request.putInHeader("img_art", img_art);
        request.putInHeader("file_desc", file_desc);
        request.putInHeader("file_ex", fileExtension);

        if (startTime != null) {
            request.putInHeader("start_time", startTime.toString());
        }
        if (endTime != null) {
            request.putInHeader("end_time", endTime.toString());
        }

        network.queue.add(request);
    }

}
