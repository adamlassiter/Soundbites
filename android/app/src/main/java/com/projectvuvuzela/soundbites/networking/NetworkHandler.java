package com.projectvuvuzela.soundbites.networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.android.volley.*;
import com.android.volley.toolbox.Volley;
import com.android.volley.Response.*;
import com.projectvuvuzela.soundbites.*;
import com.projectvuvuzela.soundbites.R;

public class NetworkHandler {

    protected RequestQueue queue;

    protected static String BASE_URL;
    private static NetworkHandler instance = new NetworkHandler();

    final public static UserRequestHandler user = new UserRequestHandler(instance);
    final public static PostRequestHandler post = new PostRequestHandler(instance);
    final public static CommentRequestHandler comment = new CommentRequestHandler(instance);
    final public static LoginRequestHandler login = new LoginRequestHandler(instance);
    final public static FeedRequestHandler feed = new FeedRequestHandler(instance);
    final public static StreamRequestHandler stream = new StreamRequestHandler(instance);

    final public static String PREFS = "soundbites";
    final public static String PREFS_USER_ID = "userID";
    final public static String PREFS_AUTH_TOKEN = "authToken";
    final public static String PREFS_USER_NAME = "userName";

    public static NetworkHandler getInstance() {
        return instance;
    }

    private NetworkHandler() {
        Resources resources = App.getContext().getResources();
        BASE_URL = resources.getString(R.string.networkingProtocol) + "://" + resources.getString(R.string.networkingHost) + ":" + resources.getInteger(R.integer.networkingPort);

        queue = Volley.newRequestQueue(App.getContext());
    }

    public interface ResponseHandler<T> {
        void onResponse(boolean success, T response, String error);
    }

    public static void setUserID(String userID, String username, String authToken) {
        SharedPreferences.Editor prefs = App.getContext().getSharedPreferences(PREFS, Context.MODE_PRIVATE).edit();
        prefs.putString(PREFS_USER_ID, userID);
        prefs.putString(PREFS_USER_NAME, username);
        prefs.putString(PREFS_AUTH_TOKEN, authToken);
        prefs.apply();
    }

    public static String getUserID() {
        SharedPreferences prefs = App.getContext().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return prefs.getString(PREFS_USER_ID, null);
    }

    public static String getUserName() {
        SharedPreferences prefs = App.getContext().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return prefs.getString(PREFS_USER_NAME, null);
    }

    public static String getAuthToken() {
        SharedPreferences prefs = App.getContext().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return prefs.getString(PREFS_AUTH_TOKEN, null);
    }

    protected static class ResponseParser<T> implements Listener<T> {
        private ResponseHandler<T> handler;

        protected ResponseParser(ResponseHandler<T> handler) {
            this.handler = handler;
        }

        public void onResponse(T response) {
            handler.onResponse(true, response, null);
        }
    }

    protected static class ErrorParser<T> implements ErrorListener {
        private ResponseHandler<T> handler;

        protected ErrorParser(ResponseHandler<T> handler) {
            this.handler = handler;
        }

        public void onErrorResponse(VolleyError error) {
            // Thanks, Volley, this is the sort of thing you should be doing for me
            String errorMessage = error.getLocalizedMessage();

            if (error instanceof TimeoutError) {
                errorMessage = "A timeout occurred.";
            } else if (error instanceof NoConnectionError) {
                errorMessage = "No internet connection.";
            } else if (error instanceof NetworkError) {
                errorMessage = "A network error occurred.";
            }

            if (error.getCause() != null) {
                errorMessage = error.getCause().getLocalizedMessage();
            }

            if (error.networkResponse != null) {
                errorMessage = new String(error.networkResponse.data);
            }

            if (errorMessage == null || errorMessage.equals("")) {
                errorMessage = "An unknown error occurred.";
            }

            handler.onResponse(false, null, errorMessage);
        }
    }

}
