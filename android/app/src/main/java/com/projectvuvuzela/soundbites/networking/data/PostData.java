package com.projectvuvuzela.soundbites.networking.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostData implements Serializable {

    @SerializedName("_id")         public String postID       = "";
    @SerializedName("user_id")     public String userID       = "";
    @SerializedName("user_nm")     public String username     = "";
    @SerializedName("post_nm")     public String postName     = "";
    @SerializedName("file_nm")     public String fileName     = "";
    @SerializedName("img_art")     public String fileImage    = "";
    @SerializedName("file_desc")   public String fileDesc     = "";
    @SerializedName("comment_ids") public String[] commentIDs = {};
    @SerializedName("time")        public double time         = 0.0;

}
