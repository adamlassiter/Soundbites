package com.projectvuvuzela.soundbites.activities;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.projectvuvuzela.soundbites.*;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.NetworkHandler.ResponseHandler;
import com.projectvuvuzela.soundbites.networking.data.*;
import com.projectvuvuzela.soundbites.views.*;

import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.TextView;

import static android.view.View.*;

public class ProfilePage extends Fragment {

    final public static String USER_ID_KEY = "userID";
    final public static String USER_NAME_KEY = "username";

    private View rootView;
    private String userID;
    private String username;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_profile_page, container, false);
        Toolbar toolbar = (Toolbar)rootView.findViewById(R.id.toolbar);
        final FloatingActionButton fab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        PostsList postlist = (PostsList)rootView.findViewById(R.id.postTable);
        TextView followerstext = (TextView)rootView.findViewById(R.id.followersText);

        // I'm starting to wish I was writing Swift…
        String argUserID = getArguments() != null ? getArguments().getString(USER_ID_KEY) : null;
        String argUsername = getArguments() != null ? getArguments().getString(USER_NAME_KEY) : null;

        userID = argUserID != null ? argUserID : NetworkHandler.getUserID();
        username = argUsername != null ? argUsername : NetworkHandler.getUserName();

        toolbar.setTitle(username);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        fab.setVisibility(GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                fab.setVisibility(GONE);

                NetworkHandler.user.addFriend(userID, new ResponseHandler<UserData>() {
                    public void onResponse(boolean success, UserData response, String error) {
                        if (success) {
                            Snackbar.make(fab, "You are now following " + username + "!", Snackbar.LENGTH_LONG).show();

                        } else {
                            fab.setVisibility(VISIBLE);
                            Snackbar.make(fab, error, Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        if (!userID.equals(NetworkHandler.getUserID())) {
            NetworkHandler.user.getPostsForUserID(NetworkHandler.getUserID(), new ResponseHandler<UserData>() {
                public void onResponse(boolean success, UserData response, String error) {
                    if (success) {
                        // If we're already friends, then don't show the add friends button
                        for (String friend : response.friendIDs) {
                            if (friend.equals(userID)) {
                                return;
                            }
                        }

                        // If we're not, then let's go ahead and show that button!
                        fab.setVisibility(VISIBLE);

                    } else {
                        System.err.println(error);
                    }
                }
            });
        }

        postlist.setLoadMoreListener(new OnClickListener() {
            public void onClick(View v) {
                loadMore(v);
            }
        });


        loadMore(getPostsList().getLoadMoreButton());
        return rootView;
    }

    public View getView() {
        return rootView;
    }

    protected void setFollowerCounts(int followers, int following, TextView followersText) {
        if (followers >= 0 && following >= 0) {
            followersText.setText(followers + " followers, " + following + " following");
        } else {
            followersText.setText("");
        }
    }

    protected void loadMore(final View view) {
        getPostsList().showLoadingIndicator(true);

        NetworkHandler.feed.getPersonalFeed(userID, new NetworkHandler.ResponseHandler<PostData[]>() {
            public void onResponse(boolean success, PostData[] response, String error) {
                getPostsList().showLoadingIndicator(false);

                if (success) {
                    for (PostData post : response) {
                        getPostsList().addView(new SoundbiteView(getActivity().getBaseContext(), post));
                    }

                } else {
                    Snackbar.make(view, "Couldn't load soundbites: " + error, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    protected Toolbar getToolbar() {
        return (Toolbar)getView().findViewById(R.id.toolbar);
    }

    protected FloatingActionButton getFab() {
        return (FloatingActionButton)getView().findViewById(R.id.fab);
    }

    protected PostsList getPostsList() {
        return (PostsList)getView().findViewById(R.id.postTable);
    }

    protected TextView getFollowersText() {
        return (TextView)getView().findViewById(R.id.followersText);
    }
}
