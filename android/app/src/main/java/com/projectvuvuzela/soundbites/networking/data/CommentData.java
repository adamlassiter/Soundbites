package com.projectvuvuzela.soundbites.networking.data;

import com.google.gson.annotations.SerializedName;

public class CommentData {

    @SerializedName("_id")     public String commentID = "";
    @SerializedName("post_id") public String postID    = "";
    @SerializedName("user_id") public String userID    = "";
    @SerializedName("user_nm") public String username  = "";
    @SerializedName("comment") public String comment   = "";

}
