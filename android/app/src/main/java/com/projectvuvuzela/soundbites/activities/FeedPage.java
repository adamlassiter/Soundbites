package com.projectvuvuzela.soundbites.activities;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.projectvuvuzela.soundbites.*;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.data.*;
import com.projectvuvuzela.soundbites.views.*;

import android.support.v4.app.Fragment;
import android.view.*;

import static android.view.View.*;
import static com.projectvuvuzela.soundbites.activities.ProfilePage.*;

public class FeedPage extends Fragment {

    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_feed, container, false);

        getPostsList().setLoadMoreListener(new OnClickListener() {
            public void onClick(View v) {
                loadMore(v);
            }
        });
        loadMore(getPostsList().getLoadMoreButton());

        return rootView;
    }

    public View getView() {
        return rootView;
    }

    protected void loadMore(final View view) {
        getPostsList().showLoadingIndicator(true);

        NetworkHandler.feed.getFriendFeed(new NetworkHandler.ResponseHandler<PostData[]>() {
            public void onResponse(boolean success, PostData[] response, String error) {
                getPostsList().showLoadingIndicator(false);

                if (success) {
                    for (PostData post : response) {
                        getPostsList().addView(new SoundbiteView(getActivity().getBaseContext(), post));
                    }

                } else {
                    Snackbar.make(view, "Couldn't load soundbites: " + error, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    protected PostsList getPostsList() {
        return (PostsList)rootView.findViewById(R.id.postTable);
    }

}
