package com.projectvuvuzela.soundbites.views;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.*;
import android.graphics.BitmapFactory;

import com.projectvuvuzela.soundbites.App;
import com.projectvuvuzela.soundbites.SoundbiteStreamer;
import com.projectvuvuzela.soundbites.Utils;
import com.projectvuvuzela.soundbites.R;
import com.projectvuvuzela.soundbites.activities.CommentsActivity;
import com.projectvuvuzela.soundbites.activities.ProfileActivity;
import com.projectvuvuzela.soundbites.networking.NetworkHandler;
import com.projectvuvuzela.soundbites.networking.data.PostData;

import static android.widget.ImageView.ScaleType.CENTER_CROP;
import static com.projectvuvuzela.soundbites.activities.ProfilePage.USER_ID_KEY;
import static com.projectvuvuzela.soundbites.activities.ProfilePage.USER_NAME_KEY;

public class SoundbiteView extends RelativeLayout {

    private Button authorButton;
    private TextView titleText;
    private TextView captionText;
    private ImageView artworkImage;
    private Button commentsButton;

    final static private int PADDING_DP = 3;
    final static private int ARTWORK_SIZE_DP = 70;

    private int padding;
    private int artworkSize;

    public SoundbiteView(Context context) {
        super(context);
        setUp();
    }

    public SoundbiteView(Context context, AttributeSet attributes) {
        super(context, attributes);
        setUp();
    }

    public SoundbiteView(Context context, PostData post) {
        super(context);
        setUp();

        importData(post);
    }

    private void setUp() {
        padding = Utils.dpToPx(PADDING_DP, getContext());
        artworkSize = Utils.dpToPx(ARTWORK_SIZE_DP, getContext());

        // Use Material's button touch-down animation
        int[] attrs = new int[] {android.R.attr.selectableItemBackground};
        TypedArray styledAttrs = getContext().obtainStyledAttributes(attrs);
        Drawable selectableItemDrawable = styledAttrs.getDrawable(0);
        styledAttrs.recycle();
        setForeground(selectableItemDrawable);

        setUpElements();
        setUpLayout();

        showComments(false);
    }

    private void setUpElements() {
        // noinspection RestrictedApi
        authorButton = new Button(new ContextThemeWrapper(getContext(), R.style.Widget_AppCompat_Button_Borderless), null, 0);
        authorButton.setText("Someone posted:");
        authorButton.setId(generateViewId());
        addView(authorButton);

        titleText = new TextView(getContext());
        titleText.setTextAppearance(android.R.style.TextAppearance_Material_Headline);
        titleText.setTextColor(getResources().getColor(R.color.postText, null));
        titleText.setId(generateViewId());
        addView(titleText);

        captionText = new TextView(getContext());
        captionText.setTextAppearance(android.R.style.TextAppearance_Material_Body1);
        captionText.setTextColor(getResources().getColor(R.color.postText, null));
        captionText.setId(generateViewId());
        addView(captionText);

        artworkImage = new ImageView(getContext());
        artworkImage.setBackgroundColor(getResources().getColor(R.color.colorAccent, null));
        artworkImage.setScaleType(CENTER_CROP);
        artworkImage.setId(generateViewId());
        addView(artworkImage);

        //noinspection RestrictedApi (lol whatevz)
        commentsButton = new Button(new ContextThemeWrapper(getContext(), R.style.Widget_AppCompat_Button_Borderless), null, 0);
        commentsButton.setText("Comments");
        commentsButton.setId(generateViewId());
        addView(commentsButton);
    }

    private void setUpLayout() {
        LayoutParams authorLayout = (LayoutParams)authorButton.getLayoutParams();
        authorLayout.addRule(ALIGN_PARENT_TOP);
        authorLayout.addRule(ALIGN_PARENT_LEFT);

        LayoutParams artworkLayout = (LayoutParams)artworkImage.getLayoutParams();
        artworkLayout.addRule(ALIGN_PARENT_LEFT);
        artworkLayout.addRule(BELOW, authorButton.getId());
        artworkLayout.setMargins(padding, 0, padding, padding);
        artworkLayout.height = artworkSize;
        artworkLayout.width = artworkSize;

        LayoutParams titleLayout = (LayoutParams)titleText.getLayoutParams();
        titleLayout.addRule(RIGHT_OF, artworkImage.getId());
        titleLayout.addRule(BELOW, authorButton.getId());

        LayoutParams captionLayout = (LayoutParams)captionText.getLayoutParams();
        captionLayout.addRule(RIGHT_OF, artworkImage.getId());
        captionLayout.addRule(BELOW, titleText.getId());

        LayoutParams commentLayout = (LayoutParams)commentsButton.getLayoutParams();
        commentLayout.setMargins(0, Utils.dpToPx(12, getContext()), 0, 0);
        commentLayout.addRule(BELOW, artworkImage.getId());
        commentLayout.addRule(BELOW, captionText.getId());
    }

    public void importData(final PostData post) {
        setAuthorText(post.username + " posted:");
        setTitle(post.postName);
        setCaption(post.fileDesc);
        setCommentCount(post.commentIDs.length);
        if (post.fileImage != null) {
            byte[] imgData = Base64.decode(post.fileImage, Base64.NO_WRAP);
            Drawable image = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(imgData, 0, imgData.length));
            setArtwork(image);
        }

        setAuthorTapHandler(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(App.getContext(), ProfileActivity.class);
                intent.putExtra(USER_ID_KEY, post.userID);
                intent.putExtra(USER_NAME_KEY, post.username);

                getContext().startActivity(intent);
            }
        });

        setCommentsTapHandler(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(App.getContext(), CommentsActivity.class);
                intent.putExtra(CommentsActivity.POST_KEY, post);
                getContext().startActivity(intent);
            }
        });

        setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                SoundbiteStreamer.getInstance().playSoundbite(NetworkHandler.stream.getStreamURL(post.postID));
            }
        });
    }

    public void setTitle(CharSequence title) {
        titleText.setText(title);
    }

    public void setCaption(CharSequence caption) {
        captionText.setText(caption);
    }

    public void setArtwork(@NonNull Drawable drawable) {
        artworkImage.setImageDrawable(drawable);
    }

    public void setArtworkTapHandler(OnClickListener listener) {
        artworkImage.setOnClickListener(listener);
    }

    public void showComments(boolean show) {
        commentsButton.setVisibility(show ? VISIBLE : GONE);
        setPadding(padding, 0, padding, show ? 0 : padding);
    }

    public void setCommentsTapHandler(OnClickListener listener) {
        commentsButton.setOnClickListener(listener);
    }

    public void setAuthorTapHandler(OnClickListener listener) {
        authorButton.setOnClickListener(listener);
    }

    public void setCommentCount(int comments) {
        showComments(true);

        if (comments == 0) {
            commentsButton.setText("No Comments");
        } else if (comments == 1) {
            commentsButton.setText("1 Comment");
        } else {
            commentsButton.setText(comments + " Comments");
        }
    }

    public void setAuthorText(CharSequence author) {
        authorButton.setText(author);
    }

    public CharSequence getAuthorText() {
        return authorButton.getText();
    }

}
