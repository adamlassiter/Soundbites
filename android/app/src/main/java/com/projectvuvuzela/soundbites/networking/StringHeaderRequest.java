package com.projectvuvuzela.soundbites.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.projectvuvuzela.soundbites.networking.NetworkHandler.*;

import org.json.JSONObject;

import java.util.*;

class StringHeaderRequest extends StringRequest {

    private Map<String, String> customHeaders = new HashMap<>();
    final private ResponseHandler<String> handler;

    public StringHeaderRequest(int method, String url, ResponseHandler<String> handler) {
        super(method, url, null, new ErrorParser<>(handler));
        this.handler = handler;
    }

    public Map<String, String> getHeaders() {
        return customHeaders;
    }

    public void putInHeader(String key, String value) {
        customHeaders.put(key, value);
    }

    protected Map<String, String> getParams() throws AuthFailureError {
        return super.getParams();
    }

    protected void deliverResponse(String response) {
        handler.onResponse(true, response, null);
    }
}
