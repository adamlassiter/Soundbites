#!/usr/bin/python3

from flask import request, make_response
from flask_restful import Resource

from dbInterface import UserDB
db = UserDB()


def headers(*args):
    return request.headers.get(*args)


class UserRequestHandler(Resource):

    # get a user's details given a user_id or user_nm
    def get(self):
        user_id = headers('user_id')
        user_nm = headers('user_nm')

        if user_id is None and user_nm is None:
            return make_response('Not enough arguments', 400)

        if user_nm and user_id is None:
            user = db.get_user_by_name(user_nm)
        else:
            user = db.get_user(user_id)

        if user:
            return user.to_dict_public()
        else:
            return make_response('User not found', 404)

    # create a new user given a username and password
    def post(self):
        user_nm = headers('user_nm')
        passwd = headers('passwd')

        if None in [user_nm, passwd]:
            return make_response('Not enough arguments', 400)

        if db.get_user_by_name(user_nm) is not None:
            return make_response('Username already in use', 303)

        user = db.create_user(user_nm, passwd, None)
        db.set_auth_token(user)
        return user.to_dict()

    # add a new friend
    def put(self):
        user_id = headers('user_id')
        friend_id = headers('friend_id')
        auth_tk = headers('auth_tk')

        if None in [user_id, friend_id, auth_tk]:
            return make_response('Not enough arguments', 400)

        user = db.get_user(user_id)
        friend = db.get_user(friend_id)

        if user and friend:
            return db.add_friend(user, friend, auth_tk).to_dict()
        else:
            return make_response('User not found', 404)

    # delete a user from the database
    def delete(self):
        pass
